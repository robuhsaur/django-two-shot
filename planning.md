* [x] Fork and clone the starter project from Django Two-Shot 

* [x] Create a new virtual environment in the repository directory for the project

* [x] Activate the virtual environment

* [x] Upgrade pip

* [x] Install django

* [x] Install black

* [x] Install flake8

* [x] Install djhtml

* [x] Install djlint

* [x] Deactivate your virtual environment

* [x] Activate your virtual environment

* [x] Use pip freeze to generate a requirements.txt file

* [ ] Create a Django project named "expenses" by running the command (see below) in the directory of your cloned repo after which your cloned repo directory should have these top-level directories and files:

    .
    ├── .git
    ├── .gitignore
    ├── .venv
    ├── expenses
    ├── manage.py
    ├── pyproject.toml
    ├── requirements.txt
    └── tests
* [ ] Run the migrate command to update the database for the migrations that come with a default Django installation

* [ ] Create a superuser if you want to access the admin
  

LE FEATURE TWOOOOO
* [ ] Create a Django app named accounts and install it in the expenses Django project in the INSTALLED_APPS list
* [ ] Create a Django app named receipts and install it in the expenses Django project in the INSTALLED_APPS list
* [ ] Run the migrations
* [ ] Create a super user



LE FEATURE THREEEE
The models
Create these three models with the specified features:

* [x] ExpenseCategory
* [ ] Receipts
* [x] Account
Note: these all belong to the receipts django app. Even though there's an Account model, this is not for user accounts, it's to hold info about an account to hold money, like a bank account or credit card account.

* [x] The ExpenseCategory model
The ExpenseCategory model is a value that we can apply to receipts like "gas" or "entertainment".

The ExpenseCategory model should have:

* [x] a name property that contains characters with a maximum length of 50 characters
* [x] an owner property that is a foreign key to the User  model with:
* [x] a related name of "categories"
* [x] a cascade deletion relation
* [x] a __str__ method that returns the name value



The Account model
The Account model is the way that we paid for it, such as with a specific credit card or a bank account.

The Account model should have:

* [x] a name property that contains characters with a maximum length of 100 characters
* [x] a number property that contains characters (not numbers) with a maximum length of 20 characters
* [x] an owner property that is a foreign key to the User model with:
* [x] a related name of "accounts"
* [x] a cascade deletion relation
* [x] a __str__ method that returns the name value




The Receipt model
The Receipt model is the primary thing that this application keeps track of for accounting purposes.

This model introduces the DecimalField, which is the kind of field that you always want to store money values in.

The Receipt model should have:

* [x] a vendor property that contains characters with a maximum length of 200 characters
* [x] a total property that is a DecimalField with:
three decimal places
* [x] a maximum of 10 digits
* [x] a tax property that is a DecimalField with:
three decimal places
* [x] a maximum of 10 digits
* [x] a date property that contains a date and time of when the transaction took place
* [x] a purchaser property that is a foreign key to the User model with:
* [x] a related name of "receipts"
* [x] a cascade deletion relation
* [x] a category property that is a foreign key to the ExpenseCategory model with:
* [x] a related name of "receipts"
* [x] a cascade deletion relation
* [x] an account property that is a foreign key to the Account model with:
* [x] a related name of "receipts"
* [x] a cascade deletion relation
* [x] allowed to be null


LE FEATUREEEE SEVEN 
* [ ] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login".
* [ ] Include the URL patterns from the accounts app in the expenses project with the prefix "accounts/".
* [ ] Create a templates directory under accounts.
* [ ] Create a registration directory under templates.
* [ ] Create an HTML template named login.html in the registration directory.
* [ ] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below).
* [ ] In the expenses settings.py file, create and set the variable             LOGIN_REDIRECT_URL to the value "home", which will redirect us to the path (not yet created) with the name "home".